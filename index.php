<?php require('_header.php') ?>


    <!-- hero -->
    <section class="hero">
      <div class="image image-overlay" style="background-image:url(https://images.unsplash.com/photo-1532408840957-031d8034aeef?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1780&q=80)"></div>
      <div class="container">
      	<div class="row">
      	  <div class="col-8 bg-dark text-white">
      	    <h1 class="mb-2">Is today the day you start making a difference?</h1>
      	  </div>
      	</div>
      </div>
    </section>
    <!-- / hero -->


    <!-- categories -->
    <section class="overlay">
      <div class="container overlay-item-top">
        <div class="row gutter-3">
          <div class="col-12">
            <div class="card">
              <div class="card-body py-2">
                <div class="row">
                  <div class="col-md-8 col-lg-10">
                    <input type="text" class="form-control form-control-minimal" placeholder="Find resources, ideas and services here" aria-label="Search">
                  </div>
                  <div class="col-md-4 col-lg-2">
                    <button type="button" class="btn btn-block btn-dark">Search</button>
                  </div>
                </div>
              </div>
              <div class="card-footer py-2 separator-top">
                <div class="tags">
                  <a href="">#trashtag</a>
                  <a href="">#carbonoffset</a>
                </div>
              </div>
            </div>
          </div>


        	<div class="col-md-6 col-lg-4">
        	  <div class="card stacked text-left" style="background: url(https://images.unsplash.com/photo-1434725039720-aaad6dd32dfe?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=450&h=450&q=80); background-repeat: none">
        	    <div class="card-body">
        	      
        	      <h3 class="card-title mt-3  mb-1 bg-dark text-white pl-1">Carbon Reduction</h3>
        	      <p class="card-text mb-2 text-bold bg-transparent bg-white opacity-8 p-1" >Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae laudantium pariatur architecto aperiam cumque eaque est.</p>
        	      <a href="/category.php" class="btn btn-dark btn-small">Learn More</a>
        	    </div>
        	  </div>
        	</div>
          <div class="col-md-6 col-lg-4">
            <div class="card stacked text-left" style="background: url(https://images.unsplash.com/photo-1552538962-40822613a09d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=450&h=450&q=60); background-repeat: none">
              <div class="card-body">
                
                <h3 class="card-title mt-3  mb-1 bg-dark text-white pl-1">Cooking</h3>
                <p class="card-text mb-2 text-bold bg-white opacity-8 p-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae laudantium pariatur architecto aperiam cumque eaque est.</p>
                <a href="/category.php" class="btn btn-dark btn-small">Learn More</a>
              </div>
            </div>
          </div>
           <div class="col-md-6 col-lg-4">
            <div class="card stacked text-left" style="background: url(https://images.unsplash.com/photo-1565192714416-418312c250c8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=450&h=450&q=80); background-repeat: none">
              <div class="card-body">
                
                <h3 class="card-title mt-3  mb-1 bg-dark text-white pl-1">Home Improvement</h3>
                <p class="card-text mb-2 text-bold bg-white opacity-8 p-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae laudantium pariatur architecto aperiam cumque eaque est.</p>
                <a href="/category.php" class="btn btn-dark btn-small">Learn More</a>
              </div>
            </div>
          </div>
          
          <div class="col-md-6 col-lg-4">
            <div class="card stacked text-left" style="background: url(https://images.unsplash.com/photo-1482542307837-cb8c51cb106f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=450&h=450&q=80); background-repeat: none">
              <div class="card-body">
                
                <h3 class="card-title mt-3  mb-1 bg-dark text-white pl-1">Carbon Reduction</h3>
                <p class="card-text mb-2 text-bold bg-white opacity-8 p-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae laudantium pariatur architecto aperiam cumque eaque est.</p>
                <a href="/category.php" class="btn btn-dark btn-small">Learn More</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4">
            <div class="card stacked text-left" style="background: url(https://images.unsplash.com/photo-1507525428034-b723cf961d3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=450&h=400&q=80); background-repeat: none">
              <div class="card-body">
                
                <h3 class="card-title mt-3  mb-1 bg-dark text-white pl-1">Carbon Reduction</h3>
                <p class="card-text mb-2 text-bold bg-white opacity-8 p-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae laudantium pariatur architecto aperiam cumque eaque est.</p>
                <a href="/category.php" class="btn btn-dark btn-small">Learn More</a>
              </div>
            </div>
          </div>
           <div class="col-md-6 col-lg-4">
            <div class="card stacked text-left" style="background: url(https://images.unsplash.com/photo-1507525428034-b723cf961d3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=450&h=400&q=80); background-repeat: none">
              <div class="card-body">
                
                <h3 class="card-title mt-3  mb-1 bg-dark text-white pl-1">Carbon Reduction</h3>
                <p class="card-text mb-2 text-bold bg-white opacity-8 p-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae laudantium pariatur architecto aperiam cumque eaque est.</p>
                <a href="/category.php" class="btn btn-dark btn-small">Learn More</a>
              </div>
            </div>
          </div> 
          <div class="col-md-6 col-lg-4">
            <div class="card stacked text-left" style="background: url(https://images.unsplash.com/photo-1507525428034-b723cf961d3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=450&h=400&q=80); background-repeat: none">
              <div class="card-body">
                
                <h3 class="card-title mt-3  mb-1 bg-dark text-white pl-1">Carbon Reduction</h3>
                <p class="card-text mb-2 text-bold bg-white opacity-8 p-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae laudantium pariatur architecto aperiam cumque eaque est.</p>
                <a href="/category.php" class="btn btn-dark btn-small">Learn More</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4">
            <div class="card stacked text-left" style="background: url(https://images.unsplash.com/photo-1507525428034-b723cf961d3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=450&h=400&q=80); background-repeat: none">
              <div class="card-body">
                
                <h3 class="card-title mt-3  mb-1 bg-dark text-white pl-1">Carbon Reduction</h3>
                <p class="card-text mb-2 text-bold bg-white opacity-8 p-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae laudantium pariatur architecto aperiam cumque eaque est.</p>
                <a href="/category.php" class="btn btn-dark btn-small">Learn More</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4">
            <div class="card stacked text-left" style="background: url(https://images.unsplash.com/photo-1507525428034-b723cf961d3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=450&h=400&q=80); background-repeat: none">
              <div class="card-body">
                
                <h3 class="card-title mt-3  mb-1 bg-dark text-white pl-1">Carbon Reduction</h3>
                <p class="card-text mb-2 text-bold bg-white opacity-8 p-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae laudantium pariatur architecto aperiam cumque eaque est.</p>
                <a href="/category.php" class="btn btn-dark btn-small">Learn More</a>
              </div>
            </div>
          </div>

        
        </div>
      </div>
    </section>
    <!-- / categories -->
<?php require('_footer.php') ?>
