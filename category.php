<?php require('_header.php') ?>



    <!-- hero -->
    <section class="hero">
            <div class="image image-overlay" style="background-image:url(https://images.unsplash.com/photo-1532408840957-031d8034aeef?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1780&q=80)"></div>
      <div class="container">
        <div class="row">
          <div class="col text-white text-center">
            <h1 class="mb-3">Category Example</h1>
            <p>Cooking is a major source of carbon emissions all over the world, from wood fires to gas burners. It's also bloody dangerous in some parts of the world. Smoke from open cooking fires kills more than eight times as many people as malaria.</p>
          </div>
        </div>
      </div>
      <div class="separator-top text-white">
        <div class="container">
          <div class="row separated">
            <div class="col-12 col-md-6 py-3">
              <div class="page-meta-stat">
                <div><b class="h2">42</b></div>
                <div><span>Articles</span></div>
              </div>
            </div>
            <div class="col-12 col-md-6 py-3">
              <div class="page-meta-stat">
                <div><b class="h2">42</b></div>
                <div><span>Tasks</span></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- / hero -->


    <!-- listing -->
    <section class="overlay">
      <div class="container overlay-item-top">
        <div class="row">
          <div class="col">
            <div class="accordion accordion-stack" id="accordionExample">

              <!-- Section -->
              <div class="card active">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      Things you can do
                    </button>
                  </h5>
                </div>
                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                  <div class="card-body pb-0">



                    <a href="/single.php" class="topic">
                      <div class="row align-items-center">
                        <div class="col-12 col-md-8 col-lg-8">
                          <h4 class="topic-title">Buy a cleaner burning stove</h4>
                        </div>
                        <div class="col-12 col-lg-4 d-none d-lg-block">
                          <div class="d-flex align-items-center">
                            <div class="mr-1">
                              <img class="avatar avatar-sm" src="../../assets/images/avatar.jpg" alt="Avatar">
                            </div>
                            <small>
                              Written by Andy Warhol
                              <time datetime="2019-11-10 20:00" class="text-muted d-block">Updated over a week ago</time>
                            </small>
                          </div>
                        </div>
                      </div>
                    </a>


                    <a href="/single.php" class="topic">
                      <div class="row align-items-center">
                        <div class="col-12 col-md-8 col-lg-8">
                          <h4 class="topic-title">Buy a cleaner burning stove</h4>
                        </div>
                        <div class="col-12 col-lg-4 d-none d-lg-block">
                          <div class="d-flex align-items-center">
                            <div class="mr-1">
                              <img class="avatar avatar-sm" src="../../assets/images/avatar.jpg" alt="Avatar">
                            </div>
                            <small>
                              Written by Andy Warhol
                              <time datetime="2019-11-10 20:00" class="text-muted d-block">Updated over a week ago</time>
                            </small>
                          </div>
                        </div>
                      </div>
                    </a>


                    <a href="/single.php" class="topic">
                      <div class="row align-items-center">
                        <div class="col-12 col-md-8 col-lg-8">
                          <h4 class="topic-title">Buy a cleaner burning stove</h4>
                        </div>
                        <div class="col-12 col-lg-4 d-none d-lg-block">
                          <div class="d-flex align-items-center">
                            <div class="mr-1">
                              <img class="avatar avatar-sm" src="../../assets/images/avatar.jpg" alt="Avatar">
                            </div>
                            <small>
                              Written by Andy Warhol
                              <time datetime="2019-11-10 20:00" class="text-muted d-block">Updated over a week ago</time>
                            </small>
                          </div>
                        </div>
                      </div>
                    </a>


                    <a href="/single.php" class="topic">
                      <div class="row align-items-center">
                        <div class="col-12 col-md-8 col-lg-8">
                          <h4 class="topic-title">Buy a cleaner burning stove</h4>
                        </div>
                        <div class="col-12 col-lg-4 d-none d-lg-block">
                          <div class="d-flex align-items-center">
                            <div class="mr-1">
                              <img class="avatar avatar-sm" src="../../assets/images/avatar.jpg" alt="Avatar">
                            </div>
                            <small>
                              Written by Andy Warhol
                              <time datetime="2019-11-10 20:00" class="text-muted d-block">Updated over a week ago</time>
                            </small>
                          </div>
                        </div>
                      </div>
                    </a>


                    <a href="/single.php" class="topic">
                      <div class="row align-items-center">
                        <div class="col-12 col-md-8 col-lg-8">
                          <h4 class="topic-title">Buy a cleaner burning stove</h4>
                        </div>
                        <div class="col-12 col-lg-4 d-none d-lg-block">
                          <div class="d-flex align-items-center">
                            <div class="mr-1">
                              <img class="avatar avatar-sm" src="../../assets/images/avatar.jpg" alt="Avatar">
                            </div>
                            <small>
                              Written by Andy Warhol
                              <time datetime="2019-11-10 20:00" class="text-muted d-block">Updated over a week ago</time>
                            </small>
                          </div>
                        </div>
                      </div>
                    </a>
                  
                  </div>
                  <div class="card-footer">
                    <a href="" class="action eyebrow">View All Articles</a>
                  </div>
                </div>
              </div>
              <!-- Section -->

              <!-- Section -->
              <div class="card ">
                <div class="card-header" id="headingTwo">
                  <h5 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                      Things you can buy
                    </button>
                  </h5>
                </div>
                <div id="collapseTwo" class="collapse " aria-labelledby="headingTwo" data-parent="#accordionExample">
                  <div class="card-body pb-0">



                    <a href="/single.php" class="topic">
                      <div class="row align-items-center">
                        <div class="col-12 col-md-8 col-lg-8">
                          <h4 class="topic-title">Buy a cleaner burning stove</h4>
                        </div>
                        <div class="col-12 col-lg-4 d-none d-lg-block">
                          <div class="d-flex align-items-center">
                            <div class="mr-1">
                              <img class="avatar avatar-sm" src="../../assets/images/avatar.jpg" alt="Avatar">
                            </div>
                            <small>
                              Written by Andy Warhol
                              <time datetime="2019-11-10 20:00" class="text-muted d-block">Updated over a week ago</time>
                            </small>
                          </div>
                        </div>
                      </div>
                    </a>


                    <a href="/single.php" class="topic">
                      <div class="row align-items-center">
                        <div class="col-12 col-md-8 col-lg-8">
                          <h4 class="topic-title">Buy a cleaner burning stove</h4>
                        </div>
                        <div class="col-12 col-lg-4 d-none d-lg-block">
                          <div class="d-flex align-items-center">
                            <div class="mr-1">
                              <img class="avatar avatar-sm" src="../../assets/images/avatar.jpg" alt="Avatar">
                            </div>
                            <small>
                              Written by Andy Warhol
                              <time datetime="2019-11-10 20:00" class="text-muted d-block">Updated over a week ago</time>
                            </small>
                          </div>
                        </div>
                      </div>
                    </a>


                    <a href="/single.php" class="topic">
                      <div class="row align-items-center">
                        <div class="col-12 col-md-8 col-lg-8">
                          <h4 class="topic-title">Buy a cleaner burning stove</h4>
                        </div>
                        <div class="col-12 col-lg-4 d-none d-lg-block">
                          <div class="d-flex align-items-center">
                            <div class="mr-1">
                              <img class="avatar avatar-sm" src="../../assets/images/avatar.jpg" alt="Avatar">
                            </div>
                            <small>
                              Written by Andy Warhol
                              <time datetime="2019-11-10 20:00" class="text-muted d-block">Updated over a week ago</time>
                            </small>
                          </div>
                        </div>
                      </div>
                    </a>


                    <a href="/single.php" class="topic">
                      <div class="row align-items-center">
                        <div class="col-12 col-md-8 col-lg-8">
                          <h4 class="topic-title">Buy a cleaner burning stove</h4>
                        </div>
                        <div class="col-12 col-lg-4 d-none d-lg-block">
                          <div class="d-flex align-items-center">
                            <div class="mr-1">
                              <img class="avatar avatar-sm" src="../../assets/images/avatar.jpg" alt="Avatar">
                            </div>
                            <small>
                              Written by Andy Warhol
                              <time datetime="2019-11-10 20:00" class="text-muted d-block">Updated over a week ago</time>
                            </small>
                          </div>
                        </div>
                      </div>
                    </a>


                    <a href="/single.php" class="topic">
                      <div class="row align-items-center">
                        <div class="col-12 col-md-8 col-lg-8">
                          <h4 class="topic-title">Buy a cleaner burning stove</h4>
                        </div>
                        <div class="col-12 col-lg-4 d-none d-lg-block">
                          <div class="d-flex align-items-center">
                            <div class="mr-1">
                              <img class="avatar avatar-sm" src="../../assets/images/avatar.jpg" alt="Avatar">
                            </div>
                            <small>
                              Written by Andy Warhol
                              <time datetime="2019-11-10 20:00" class="text-muted d-block">Updated over a week ago</time>
                            </small>
                          </div>
                        </div>
                      </div>
                    </a>
                  
                  </div>
                  <div class="card-footer">
                    <a href="" class="action eyebrow">View All Articles</a>
                  </div>
                </div>
              </div>
              <!-- Section -->

            </div>

          </div>
        </div>
      </div>
    </section>
    <!-- / listing -->




<?php require('_footer.php') ?>
