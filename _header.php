<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/vendor.css" />
    <link rel="stylesheet" href="/assets/css/style.css" />

    <title>Awesome Earth</title>
  </head>
  <body id="top">


    <!-- header -->
    <header class="header header-sticky header-minimal-dark">
      <div class="container">
        <div class="row">
          <nav class="navbar navbar-expand-lg navbar-dark">
            <a href="/" class="navbar-brand">Awesome.earth</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                  <a class="nav-link active" href="/">Home</a>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="dropdown-1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Example Link
                  </a>
                  <div class="dropdown-menu" aria-labelledby="dropdown-1">
                    <a class="dropdown-item" href="#">Demo Link 1</a>
                    <a class="dropdown-item" href="#">Demo Link 2</a>
                    <a class="dropdown-item" href="#">Demo Link 3</a>
                    <a class="dropdown-item" href="#">Demo Link 4</a>
                  </div>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link  dropdown-toggle" href="#" id="dropdown-2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Another link
                  </a>
                  <div class="dropdown-menu" aria-labelledby="dropdown-2">
                    <a class="dropdown-item active" href="#">Demo Link 1</a>
                    <a class="dropdown-item" href="#">Demo Link 2</a>
                    <a class="dropdown-item" href="#">Demo Link 3</a>

                  </div>
                </li>

              </ul>
              <ul class="navbar-nav">
                <li class="nav-item">
                  <a class="nav-link" href="#">More Links</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Another Link</a>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    </header>
    <!-- / header -->
