

    <!-- go top -->
    <div class="half">
      <span class="half-bg bg-white"></span>
      <div class="container">
        <div class="row">
          <div class="col text-center">
            <div class="btn-frame bg-light">
              <a data-scroll href="#top" class="btn btn-top btn-white btn-ico btn-lg btn-rounded"><i class="icon-arrow-up2 fs-22"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- / go top -->


<!-- You can use these as you want, not with both on the page stacked like this... :p -->

    <!-- subscribe -->
    <div class="half bg-white">
      <span class="half-bg bg-dark"></span>
      <div class="container">
        <div class="row">
          <div class="col">
            <div class="boxed no-border p-4 bg-secondary">
              <div class="row gutter-2 align-items-center">
                <div class="col-12 col-md-6 text-white">
                  <h3>Get on our mailing list, save the world!</h3>
                </div>
                <div class="col-12 col-md-6">
                  <input type="email" class="form-control form-control-inverted form-control-rounded" id="exampleInputEmail1" placeholder="Enter your email">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- / subscribe -->


    <!-- cta -->
    <div class="half bg-white">
      <span class="half-bg bg-dark"></span>
      <div class="container">
        <div class="row">
          <div class="col">
            <div class="boxed no-border bg-secondary text-white">
              <div class="row gutter-0 align-items-center separated">
                <div class="col-12 col-md-6">
                  <div class="d-flex align-items-center justify-content-center py-5">
                    <div class="mr-3">
                      <i class="icon-message-circle fs-24 text-white"></i>
                    </div>
                    <div>
                      <h3 class="mb-0">Live Chat</h3>
                      <a href="" class="eyebrow eyebrow-sm link">Chat with cool people who want to help, or some other CTA</a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="d-flex align-items-center justify-content-center py-5">
                    <div class="mr-3">
                      <i class="icon-mail fs-24 text-white"></i>
                    </div>
                    <div>
                      <h3 class="mb-0">Get in touch</h3>
                      <a href="" class="eyebrow eyebrow-sm action">Submit a ticket</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- / cta -->

    <!-- footer -->
    <footer class="bg-dark">
      <div class="container">
        <div class="row gutter-3">
          <div class="col-12 col-md-2">
            <a href="/" class="text-white">Awesom.earth</a>
          </div>
          <div class="col-12 col-md-6 text-white">
            <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <div class="row">
              <div class="col">
                <ul class="list-group list-group-minimal">
                  <li class="list-group-item"><a href="" class="link">Link </a></li>
                  <li class="list-group-item"><a href="" class="link">Link </a></li>
                  <li class="list-group-item"><a href="" class="link">Link </a></li>
                </ul>
              </div>
              <div class="col">
                <ul class="list-group list-group-minimal">
                  <li class="list-group-item"><a href="" class="link">REST API</a></li>
                  <li class="list-group-item"><a href="" class="link">GRAPH API</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-4 col-lg-2 ml-auto text-md-right">
            <div class="dropdown">
              <button class="btn btn-inverted btn-block dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                English
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">British</a>
                <a class="dropdown-item" href="#">French</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!-- / footer -->

    <script src="/assets/js/vendor.min.js"></script>
    <script src="/assets/js/app.js"></script>
  </body>
</html>