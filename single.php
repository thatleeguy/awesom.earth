<?php require('_header.php') ?>


    <!-- hero -->
    <section class="hero ">
      <div class="image image-overlay" style="background-image:url(https://images.unsplash.com/photo-1532408840957-031d8034aeef?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1780&q=80)"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 text-white mb-3">
            <h1 class="h2 pb-1">Some Task</h1>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb breadcrumb-minimal">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="/category.php">Category</a></li>
                <li class="breadcrumb-item active" aria-current="page">Some Task</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </section>
    <!-- / hero -->


    <!-- sidebar layout -->
    <section class="overlay">
      <div class="container overlay-item-top">
        <div class="row">
          <div class="col">
            <div class="content boxed">
              <div class="row separated">

                <!-- content -->
                <article class="col-md-8 content-body">


                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id illo eveniet blanditiis aliquam voluptatem quaerat sapiente alias ex pariatur quasi, ipsa dolorem impedit nemo eius, hic est magnam quisquam soluta! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id illo eveniet blanditiis aliquam voluptatem quaerat sapiente alias ex pariatur quasi, ipsa dolorem impedit nemo eius, hic est magnam quisquam soluta!</p>

                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id illo eveniet blanditiis aliquam voluptatem quaerat sapiente alias ex pariatur quasi, ipsa dolorem impedit nemo eius, hic est magnam quisquam soluta! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id illo eveniet blanditiis aliquam voluptatem quaerat sapiente alias ex pariatur quasi, ipsa dolorem impedit nemo eius, hic est magnam quisquam soluta!</p>

                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id illo eveniet blanditiis aliquam voluptatem quaerat sapiente alias ex pariatur quasi, ipsa dolorem impedit nemo eius, hic est magnam quisquam soluta! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id illo eveniet blanditiis aliquam voluptatem quaerat sapiente alias ex pariatur quasi, ipsa dolorem impedit nemo eius, hic est magnam quisquam soluta!</p>

                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id illo eveniet blanditiis aliquam voluptatem quaerat sapiente alias ex pariatur quasi, ipsa dolorem impedit nemo eius, hic est magnam quisquam soluta! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id illo eveniet blanditiis aliquam voluptatem quaerat sapiente alias ex pariatur quasi, ipsa dolorem impedit nemo eius, hic est magnam quisquam soluta!</p>








                  <footer class="content-footer text-center">
                    <h4 class="fs-18">Was this article helpfull ?</h4>
                    <div class="btn-group btn-group-toggle mt-1" data-toggle="buttons">
                      <label class="btn btn-sm btn-outline-primary active">
                        <input type="radio" name="options" id="option1"checked> Yes
                      </label>
                      <label class="btn btn-sm btn-outline-primary">
                        <input type="radio" name="options" id="option2"> No
                      </label>
                    </div>
                  </footer>

                </article>
                <!-- / content -->


                <!-- sidebar -->
                <aside class="col-md-4 content-aside bg-light">
                  <div class="widget">
                    <h3 class="widget-title">Related Posts</h3>
                    <div class="list-group list-group-related">
                      <a href="#" class="list-group-item list-group-item-action d-flex align-items-center active">
                        <i class="fs-20 icon-file-text2 text-secondary mr-1"></i>
                        Other related items from this category
                      </a>
                      <a href="#" class="list-group-item list-group-item-action d-flex align-items-center active">
                        <i class="fs-20 icon-file-text2 text-secondary mr-1"></i>
                        Other related items from this category
                      </a>
                      <a href="#" class="list-group-item list-group-item-action d-flex align-items-center active">
                        <i class="fs-20 icon-file-text2 text-secondary mr-1"></i>
                        Other related items from this category
                      </a>
                      <a href="#" class="list-group-item list-group-item-action d-flex align-items-center active">
                        <i class="fs-20 icon-file-text2 text-secondary mr-1"></i>
                        Other related items from this category
                      </a>
                      <a href="#" class="list-group-item list-group-item-action d-flex align-items-center active">
                        <i class="fs-20 icon-file-text2 text-secondary mr-1"></i>
                        Other related items from this category
                      </a>
                    </div>
                  </div>

                </aside>
                <!-- / sidebar -->


              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- / sidebar layout -->




    <!-- interpost -->
    <section class="bg-dark text-white separator-bottom p-0">
      <div class="container">
        <div class="row gutter-0">
          <div class="col-12 col-md-6">
            <h4 class="interpost interpost-prev"><a href="">Some other task of interest</a></h4>
          </div>
          <div class="col-12 col-md-6 text-right">
            <h4 class="interpost interpost-next"><a href="">Some other task of interest</a></h4>
          </div>
        </div>
      </div>
    </section>
    <!-- / interpost -->

<?php require('_footer.php') ?>
